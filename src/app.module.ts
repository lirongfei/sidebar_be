import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { RedisModule } from '@nestjs-modules/ioredis';

@Module({
  imports: [
    NestjsFormDataModule,
    RedisModule.forRoot({
      type: 'single',
      url: 'redis://redis.zaijia.work:6379',
      options: {
        password: 'xixifun'
      }
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
