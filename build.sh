#!/bin/bash
export PATH=/root/.nvm/versions/node/v16.17.0/bin:$PATH
npm set registry http://npm.zaijia.work/
yarn
npm run build
export NODE_ENV=production
forever start dist/main.js